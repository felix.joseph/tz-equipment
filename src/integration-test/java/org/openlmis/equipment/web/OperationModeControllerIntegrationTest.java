/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.equipment.web;

import static java.util.Collections.singletonList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;
import static org.openlmis.equipment.i18n.PermissionMessageKeys.ERROR_NO_FOLLOWING_PERMISSION;

import com.jayway.restassured.response.Response;
import guru.nidi.ramltester.junit.RamlMatchers;
import java.util.Optional;
import java.util.UUID;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openlmis.equipment.domain.OperationMode;
import org.openlmis.equipment.dto.OperationModeDto;
import org.openlmis.equipment.i18n.OperationModeMessageKeys;
import org.openlmis.equipment.service.PermissionService;
import org.openlmis.equipment.util.PageDto;
import org.openlmis.equipment.util.Pagination;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

@SuppressWarnings ("PMD.TooManyMethods")
public class OperationModeControllerIntegrationTest extends BaseWebIntegrationTest {

  private static final String RESOURCE_URL = "/api/equipmentOperationModes";
  private static final String RESOURCE_URL_WITH_ID = RESOURCE_URL + "/{id}";

  private final UUID operationModeId = UUID.randomUUID();
  private OperationMode operationMode;
  private OperationModeDto operationModeDto;

  @Before
  public void setUp() {
    mockUserAuthenticated();

    operationMode = new OperationMode(
        "Freezer", "freezer", 1, "A new freezer", true);
    operationModeDto = toDto(operationMode);
    given(operationModeRepository.save(any(OperationMode.class)))
        .willAnswer(new SaveAnswer<OperationMode>());
  }

  @Test
  public void shouldCreateNewOperationMode() {
    OperationModeDto response = postOperationMode()
        .then()
        .statusCode(201)
        .extract().as(OperationModeDto.class);
    checkResponseAndRaml(response);
  }

  @Test
  public void shouldUpdateOperationMode() {
    OperationModeDto response = putOperationMode(operationModeId)
        .then()
        .statusCode(200)
        .extract().as(OperationModeDto.class);
    assertNotNull(response);
    Assert.assertThat(RAML_ASSERT_MESSAGE, restAssured.getLastReport(),
        RamlMatchers.hasNoViolations());
  }

  @Test
  public void shouldCreateNewOperationModeIfDoesNotExistForPutRequest() {
    UUID id = UUID.randomUUID();
    OperationModeDto response = putOperationMode(id)
        .then()
        .statusCode(200)
        .extract().as(OperationModeDto.class);
    assertNotNull(response);
    Assert.assertThat(RAML_ASSERT_MESSAGE, restAssured.getLastReport(),
        RamlMatchers.hasNoViolations());
  }

  @Test
  public void shouldGetOperationModeWithId() {
    when(operationModeRepository.findById(operationModeId))
        .thenReturn(Optional.ofNullable(operationMode));

    OperationModeDto response = getOperationMode(operationModeId)
        .then()
        .statusCode(200)
        .extract().as(OperationModeDto.class);
    assertEquals(operationModeDto, response);
    Assert.assertThat(RAML_ASSERT_MESSAGE, restAssured.getLastReport(),
        RamlMatchers.hasNoViolations());
  }

  @Test
  public void shouldReturnAllOperationModes() {
    when(operationModeRepository.findAll(any(Pageable.class)))
        .thenReturn(Pagination.getPage(singletonList(operationMode),
            PageRequest.of(0, 1), 1));
    PageDto resultPage = getAllOperationModes()
        .then()
        .statusCode(200)
        .extract().as(PageDto.class);

    assertThat(resultPage.getContent().size()).isEqualTo(1);
    assertThat(resultPage.hasContent()).isTrue();
    Assert.assertThat(RAML_ASSERT_MESSAGE,
        restAssured.getLastReport(), RamlMatchers.hasNoViolations());
  }

  @Test
  public void shouldReturnBadRequestIfPostOperationModeNameValidationFails() {
    operationModeDto.setName(null);
    postOperationMode()
        .then()
        .statusCode(400)
        .body(MESSAGE,
            equalTo(getMessage(OperationModeMessageKeys.ERROR_OPERATION_MODE_NAME_IS_REQUIRED)));
  }

  @Test
  public void shouldReturnBadRequestIfPostOperationModeCodeValidationFails() {
    operationModeDto.setCode(null);
    postOperationMode()
        .then()
        .statusCode(400)
        .body(MESSAGE,
            equalTo(getMessage(OperationModeMessageKeys.ERROR_OPERATION_MODE_CODE_IS_REQUIRED)));
  }

  @Test
  public void shouldReturnBadRequestIfPutOperationModeNameValidationFails() {
    operationModeDto.setName(null);
    putOperationMode(operationModeId)
        .then()
        .statusCode(400)
        .body(MESSAGE,
            equalTo(getMessage(OperationModeMessageKeys.ERROR_OPERATION_MODE_NAME_IS_REQUIRED)));
  }

  @Test
  public void shouldReturnBadRequestIfPutOperationModeCodeValidationFails() {
    operationModeDto.setCode(null);
    putOperationMode(operationModeId)
        .then()
        .statusCode(400)
        .body(MESSAGE,
            equalTo(getMessage(OperationModeMessageKeys.ERROR_OPERATION_MODE_CODE_IS_REQUIRED)));
  }

  @Test
  public void shouldReturnNotFoundRequestWhenGetOperationModeIsAbsent() {
    UUID id = UUID.randomUUID();
    getOperationMode(id)
        .then()
        .statusCode(404)
        .body(MESSAGE,
            equalTo(getMessage(OperationModeMessageKeys.ERROR_OPERATION_MODE_NOT_FOUND)));
    Assert.assertThat(RAML_ASSERT_MESSAGE, restAssured.getLastReport(),
        RamlMatchers.hasNoViolations());
  }

  @Test
  public void shouldReturnUnauthorizedWhenPostButUserHasNoManagePermission() {
    doThrow(mockPermissionException(PermissionService.CCE_MANAGE))
        .when(permissionService).canManageCce();

    postOperationMode()
        .then()
        .statusCode(403)
        .body(MESSAGE, equalTo(getMessage(ERROR_NO_FOLLOWING_PERMISSION,
            PermissionService.CCE_MANAGE)));
    Assert.assertThat(RAML_ASSERT_MESSAGE, restAssured.getLastReport(),
        RamlMatchers.hasNoViolations());
  }

  @Test
  public void shouldReturnUnauthorizedWhenUpdateButUserHasNoManagePermission() {
    doThrow(mockPermissionException(PermissionService.CCE_MANAGE))
        .when(permissionService).canManageCce();

    putOperationMode(operationModeId)
        .then()
        .statusCode(403)
        .body(MESSAGE, equalTo(getMessage(ERROR_NO_FOLLOWING_PERMISSION,
            PermissionService.CCE_MANAGE)));
    Assert.assertThat(RAML_ASSERT_MESSAGE, restAssured.getLastReport(),
        RamlMatchers.hasNoViolations());
  }

  private Response postOperationMode() {
    return restAssured
        .given()
        .header(HttpHeaders.AUTHORIZATION, getTokenHeader())
        .contentType(MediaType.APPLICATION_JSON_VALUE)
        .body(operationModeDto)
        .when()
        .post(RESOURCE_URL);
  }

  private Response putOperationMode(UUID id) {
    return restAssured
        .given()
        .header(HttpHeaders.AUTHORIZATION, getTokenHeader())
        .contentType(MediaType.APPLICATION_JSON_VALUE)
        .pathParam("id", id)
        .body(operationModeDto)
        .when()
        .put(RESOURCE_URL_WITH_ID);
  }

  private Response getOperationMode(UUID id) {
    return restAssured
        .given()
        .header(HttpHeaders.AUTHORIZATION, getTokenHeader())
        .contentType(MediaType.APPLICATION_JSON_VALUE)
        .pathParam("id", id)
        .when()
        .get(RESOURCE_URL_WITH_ID);
  }

  private Response getAllOperationModes() {
    return restAssured
        .given()
        .header(HttpHeaders.AUTHORIZATION, getTokenHeader())
        .when()
        .get(RESOURCE_URL);
  }

  private void checkResponseAndRaml(OperationModeDto response) {
    assertEquals(operationMode.getName(), response.getName());
    assertEquals(operationMode.getCode(), response.getCode());
    assertEquals(operationMode.getDisplayOrder(), response.getDisplayOrder());
    assertEquals(operationMode.getDescription(), response.getDescription());
    assertEquals(operationMode.getActive(), response.getActive());

    Assert.assertThat(RAML_ASSERT_MESSAGE, restAssured.getLastReport(),
        RamlMatchers.hasNoViolations());
  }

  /**
   * method to convert operationMode domain to dto.
   *
   * @param domain operationMode object.
   * @return OperationModeDto
   */
  public OperationModeDto toDto(OperationMode domain) {
    OperationModeDto dto = new OperationModeDto();
    domain.export(dto);
    return dto;
  }

}
