/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.equipment.web;

import static java.util.Collections.singletonList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;
import static org.openlmis.equipment.i18n.PermissionMessageKeys.ERROR_NO_FOLLOWING_PERMISSION;

import com.jayway.restassured.response.Response;
import guru.nidi.ramltester.junit.RamlMatchers;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openlmis.equipment.domain.Discipline;
import org.openlmis.equipment.dto.DisciplineDto;
import org.openlmis.equipment.i18n.DisciplineMessageKeys;
import org.openlmis.equipment.service.PermissionService;
import org.openlmis.equipment.util.PageDto;
import org.openlmis.equipment.util.Pagination;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

@SuppressWarnings("PMD.TooManyMethods")
public class DisciplineControllerIntegrationTest extends BaseWebIntegrationTest {
  private static final String RESOURCE_URL = "/api/disciplines";
  private static final String RESOURCE_URL_WITH_ID = RESOURCE_URL + "/{id}";
  private DisciplineDto disciplineDto;
  private Discipline discipline;
  private UUID disciplineId = UUID.randomUUID();

  @Before
  public void initialize() {
    mockUserAuthenticated();

    discipline = new Discipline("chemistry","code",10,false);
    disciplineDto = toDto(discipline);
    when(disciplineRepository.save(any(Discipline.class)))
        .thenAnswer(new SaveAnswer<Discipline>());
  }

  @Test
  public void shouldCreateNewDiscipline() {
    DisciplineDto response = postDiscipline(disciplineDto)
              .then()
              .statusCode(201)
              .extract().as(DisciplineDto.class);
    checkResponseAndRaml(response);
  }

  @Test
  public void shouldReturnBadRequestWhenCreateNotUnique() {
    List<Discipline> disciplineList = new ArrayList<Discipline>();
    disciplineList.add(discipline);
    when(disciplineRepository.findByName(disciplineDto.getName()))
            .thenReturn(disciplineList);

    postDiscipline(disciplineDto)
            .then()
            .statusCode(400)
            .body(MESSAGE, equalTo(getMessage(DisciplineMessageKeys.ERROR_DISCIPLINE_NAME_EXISTS)));
  }

  @Test
  public void shouldUpdateDiscipline() {
    DisciplineDto response = putDiscipline(disciplineId,disciplineDto)
            .then()
            .statusCode(200)
            .extract().as(DisciplineDto.class);
    assertThat(response).isNotNull();
    Assert.assertThat(RAML_ASSERT_MESSAGE, restAssured.getLastReport(),
            RamlMatchers.hasNoViolations());
  }

  @Test
  public void shouldReturnBadRequestWhenPutWithInvalidPayload() {
    DisciplineDto invalidDto = new DisciplineDto();
    invalidDto.setCode("code");
    putDiscipline(disciplineId,invalidDto)
            .then()
            .statusCode(400)
            .body(MESSAGE, equalTo(getMessage(DisciplineMessageKeys.NAME_IS_REQUIRED)));
  }

  @Test
  public void shouldGetDisciplineWithId() {
    when(disciplineRepository.findById(disciplineId))
            .thenReturn(Optional.ofNullable(discipline));

    DisciplineDto response = getDiscipline(disciplineId)
            .then()
            .statusCode(200)
            .extract().as(DisciplineDto.class);
    assertThat(response).isEqualTo(disciplineDto);
    Assert.assertThat(RAML_ASSERT_MESSAGE, restAssured.getLastReport(),
            RamlMatchers.hasNoViolations());
  }

  @Test
  public void shouldReturnAllDisciplines() {
    when(disciplineRepository.findAll(any(Pageable.class)))
            .thenReturn(Pagination.getPage(singletonList(discipline),
                    PageRequest.of(0, 1), 1));
    PageDto resultPage = getAllDisciplines()
            .then()
            .statusCode(200)
            .extract().as(PageDto.class);

    assertThat(resultPage.getContent().size()).isEqualTo(1);
    assertThat(resultPage.hasContent()).isTrue();
    Assert.assertThat(RAML_ASSERT_MESSAGE,
            restAssured.getLastReport(), RamlMatchers.hasNoViolations());
  }

  @Test
  public void shouldReturnBadRequestIfPostDisciplineValidationFails() {
    DisciplineDto invalidDto = new DisciplineDto();
    invalidDto.setCode("code");
    postDiscipline(invalidDto)
            .then()
            .statusCode(400)
            .body(MESSAGE, equalTo(getMessage(DisciplineMessageKeys.NAME_IS_REQUIRED)));
  }

  @Test
  public void shouldReturnNotFoundRequestWhenGetDisciplineIsAbsent() {
    UUID id = UUID.randomUUID();
    getDiscipline(id)
            .then()
            .statusCode(404)
            .body(MESSAGE, equalTo(getMessage(DisciplineMessageKeys.ERROR_DISCIPLINE_NOT_FOUND)));
    Assert.assertThat(RAML_ASSERT_MESSAGE, restAssured.getLastReport(),
            RamlMatchers.hasNoViolations());
  }

  @Test
  public void shouldReturnUnauthorizedWhenPostButUserHasNoManagePermission() {
    doThrow(mockPermissionException(PermissionService.CCE_MANAGE))
    .when(permissionService).canManageCce();

    postDiscipline(disciplineDto)
            .then()
            .statusCode(403)
            .body(MESSAGE, equalTo(getMessage(ERROR_NO_FOLLOWING_PERMISSION,
                    PermissionService.CCE_MANAGE)));
    Assert.assertThat(RAML_ASSERT_MESSAGE, restAssured.getLastReport(),
            RamlMatchers.hasNoViolations());
  }

  @Test
  public void shouldReturnUnauthorizedWhenUpdateButUserHasNoManagePermission() {
    doThrow(mockPermissionException(PermissionService.CCE_MANAGE))
            .when(permissionService).canManageCce();

    putDiscipline(disciplineId,disciplineDto)
            .then()
            .statusCode(403)
            .body(MESSAGE, equalTo(getMessage(ERROR_NO_FOLLOWING_PERMISSION,
                    PermissionService.CCE_MANAGE)));
    Assert.assertThat(RAML_ASSERT_MESSAGE, restAssured.getLastReport(),
            RamlMatchers.hasNoViolations());
  }

  /**
  *method to convert discpline domain to dto.
  * @param domain discipline object.
  * @return DisciplineDto
  */
  public DisciplineDto toDto(Discipline domain) {
    DisciplineDto dto = new DisciplineDto();
    domain.export(dto);
    return dto;
  }

  private Response postDiscipline(DisciplineDto dto) {
    return restAssured
            .given()
            .header(HttpHeaders.AUTHORIZATION, getTokenHeader())
            .contentType(MediaType.APPLICATION_JSON_VALUE)
            .body(dto)
            .when()
            .post(RESOURCE_URL);
  }

  private void checkResponseAndRaml(DisciplineDto response) {
    assertThat(discipline.getName()).isEqualTo(response.getName());
    assertThat(discipline.getCode()).isEqualTo(response.getCode());
    assertThat(discipline.getDisplayOrder()).isEqualTo(response.getDisplayOrder());
    Assert.assertThat(RAML_ASSERT_MESSAGE, restAssured.getLastReport(),
            RamlMatchers.hasNoViolations());
  }

  private Response getAllDisciplines() {
    return restAssured
            .given()
            .header(HttpHeaders.AUTHORIZATION, getTokenHeader())
            .when()
            .get(RESOURCE_URL);
  }

  private Response putDiscipline(UUID id,DisciplineDto dto) {
    return restAssured
            .given()
            .header(HttpHeaders.AUTHORIZATION, getTokenHeader())
            .contentType(MediaType.APPLICATION_JSON_VALUE)
            .pathParam("id", id)
            .body(dto)
            .when()
            .put(RESOURCE_URL_WITH_ID);
  }

  private Response getDiscipline(UUID id) {
    return restAssured
            .given()
            .header(HttpHeaders.AUTHORIZATION, getTokenHeader())
            .contentType(MediaType.APPLICATION_JSON_VALUE)
            .pathParam("id", id)
            .when()
            .get(RESOURCE_URL_WITH_ID);
  }
}
