/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.equipment.web;

import static java.util.Collections.singletonList;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;
import static org.openlmis.equipment.service.ResourceNames.BASE_PATH;

import guru.nidi.ramltester.junit.RamlMatchers;

import java.util.Optional;
import java.util.UUID;

import org.junit.Assert;
import org.junit.Test;

import org.openlmis.equipment.domain.EquipmentCategory;
import org.openlmis.equipment.dto.EquipmentCategoryDto;
import org.openlmis.equipment.i18n.DisciplineMessageKeys;
import org.openlmis.equipment.service.PermissionService;
import org.openlmis.equipment.util.EquipmentCategoryDataBuilder;
import org.openlmis.equipment.util.PageDto;
import org.openlmis.equipment.util.Pagination;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

public class EquipmentCategoryControllerIntegrationTest extends BaseWebIntegrationTest {
  static final String RESOURCE_PATH = BASE_PATH + "/equipmentCategories";
  private static final String ID_URL = RESOURCE_PATH + "/{id}";
  private static final String EQUIPMENT_CATEGORY_NAME = "name";
  private static final String EQUIPMENT_CATEGORY_CODE = "code";

  private  EquipmentCategory equipmentCategory;
  private  EquipmentCategoryDto equipmentCategoryDto;
  private  UUID equipmentCategoryId;
  private  Pageable pageable;

  /**
   * Constructor for test class.
   */
  public EquipmentCategoryControllerIntegrationTest() {
    equipmentCategory = new EquipmentCategoryDataBuilder()
            .withCode(EQUIPMENT_CATEGORY_CODE)
            .withName(EQUIPMENT_CATEGORY_NAME)
            .withActive(true)
            .withDisplayOrder(1)
            .buildAsNew();

    equipmentCategoryDto = new EquipmentCategoryDto();
    equipmentCategory.export(equipmentCategoryDto);
    equipmentCategoryId = UUID.randomUUID();
    pageable = PageRequest.of(0, 10);
  }

  @Test
  public void shouldReturnEquipmentCategoryPage() {
    doReturn(Pagination.getPage(singletonList(equipmentCategory), pageable))
            .when(equipmentCategoryRepository).findAll();

    PageDto resultPage = restAssured.given()
            .header(HttpHeaders.AUTHORIZATION, getTokenHeader())
            .when()
            .get(RESOURCE_PATH)
            .then()
            .statusCode(200)
            .extract().as(PageDto.class);

    assertEquals(1, resultPage.getContent().size());
    assertThat(RAML_ASSERT_MESSAGE, restAssured.getLastReport(), RamlMatchers.hasNoViolations());
  }

  @Test
  public void shouldGetEquipmentCategory() {
    given(equipmentCategoryRepository.findById(equipmentCategoryId))
            .willReturn(Optional.of(equipmentCategory));

    EquipmentCategoryDto response = restAssured
            .given()
            .header(HttpHeaders.AUTHORIZATION, getTokenHeader())
            .pathParam("id", equipmentCategoryId)
            .when()
            .get(ID_URL)
            .then()
            .statusCode(200)
            .extract().as(EquipmentCategoryDto.class);

    assertEquals(EQUIPMENT_CATEGORY_NAME, response.getName());
    assertEquals(EQUIPMENT_CATEGORY_CODE, response.getCode());
    assertThat(RAML_ASSERT_MESSAGE, restAssured.getLastReport(), RamlMatchers.hasNoViolations());
  }

  @Test
  public void getShouldReturnNotFoundForNonExistingEquipmentCategory() {

    restAssured
            .given()
            .header(HttpHeaders.AUTHORIZATION, getTokenHeader())
            .pathParam("id", equipmentCategoryId)
            .when()
            .get(ID_URL)
            .then()
            .statusCode(404);

    assertThat(RAML_ASSERT_MESSAGE, restAssured.getLastReport(), RamlMatchers.hasNoViolations());
  }

  @Test
  public void shouldPostEquipmentCategory() {
    given(equipmentCategoryRepository.save(any()))
            .willReturn(equipmentCategory);

    EquipmentCategoryDto response = restAssured
            .given()
            .header(HttpHeaders.AUTHORIZATION, getTokenHeader())
            .contentType(MediaType.APPLICATION_JSON_VALUE)
            .body(equipmentCategoryDto)
            .when()
            .post(RESOURCE_PATH)
            .then()
            .statusCode(201)
            .extract().as(EquipmentCategoryDto.class);

    assertEquals(EQUIPMENT_CATEGORY_NAME, response.getName());
    assertEquals(EQUIPMENT_CATEGORY_CODE, response.getCode());
    assertThat(RAML_ASSERT_MESSAGE, restAssured.getLastReport(), RamlMatchers.hasNoViolations());
  }

  @Test
  public void shouldReturnBadRequestWhenPostEmptyFields() {
    EquipmentCategoryDto invalidDto = new EquipmentCategoryDto();
    invalidDto.setCode("code");
    restAssured
            .given()
            .header(HttpHeaders.AUTHORIZATION, getTokenHeader())
            .contentType(MediaType.APPLICATION_JSON_VALUE)
            .body(invalidDto)
            .when()
            .post(RESOURCE_PATH)
            .then()
            .statusCode(400)
            .body(MESSAGE, equalTo(getMessage(
                    DisciplineMessageKeys.ERROR_MISSING_MANDATORY_ITEMS)));

  }

  @Test
  public void shouldReturnUnauthorizedWhenPostButUserHasNoManagePermission() {
    doThrow(mockPermissionException(PermissionService.CCE_MANAGE))
            .when(permissionService).canManageCce();

    restAssured
            .given()
            .header(HttpHeaders.AUTHORIZATION, getTokenHeader())
            .contentType(MediaType.APPLICATION_JSON_VALUE)
            .body(equipmentCategoryDto)
            .when()
            .post(RESOURCE_PATH)
            .then()
            .statusCode(403);

    Assert.assertThat(RAML_ASSERT_MESSAGE, restAssured.getLastReport(),
            RamlMatchers.hasNoViolations());
  }

  @Test
  public void shouldReturnUnauthorizedWhenUpdateButUserHasNoManagePermission() {
    doThrow(mockPermissionException(PermissionService.CCE_MANAGE))
            .when(permissionService).canManageCce();

    restAssured
            .given()
            .header(HttpHeaders.AUTHORIZATION, getTokenHeader())
            .contentType(MediaType.APPLICATION_JSON_VALUE)
            .pathParam("id", equipmentCategoryId)
            .body(equipmentCategoryDto)
            .when()
            .put(ID_URL)
            .then()
            .statusCode(403);

    Assert.assertThat(RAML_ASSERT_MESSAGE, restAssured.getLastReport(),
            RamlMatchers.hasNoViolations());
  }


}
