/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.equipment.repository;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;
import java.util.Set;
import org.junit.Test;
import org.openlmis.equipment.domain.EquipmentType;
import org.openlmis.equipment.util.EquipmentTyeDataBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

public class EquipmentTypeRepositoryIntegrationTest extends
        BaseCrudRepositoryIntegrationTest<EquipmentType> {

  @Autowired
  private EquipmentTypeRepository repository;

  private Pageable pageable;

  private EquipmentType equipmentType;

  private static final String EQUIPMENT_TYPE_NAME = "Name";
  private static final String EQUIPMENT_TYPE_CODE = "Code";


  @Override
  EquipmentTypeRepository getRepository() {
    return this.repository;
  }

  @Override
  EquipmentType generateInstance() {
    pageable = PageRequest.of(0, 10);

    return new EquipmentTyeDataBuilder()
            .withActive(true)
            .withCode("code")
            .withName("name")
            .withBioChemistry(true)
            .withColdChain(true)
            .buildAsNew();
  }

  private EquipmentType generateEquipmentType() {
    return new EquipmentTyeDataBuilder()
            .withName(EQUIPMENT_TYPE_NAME)
            .withCode(EQUIPMENT_TYPE_CODE)
            .withActive(true)
            .withBioChemistry(true)
            .withColdChain(true)
            .buildAsNew();
  }


  @Test
  public void shouldFindByName() {
    equipmentType = generateEquipmentType();
    repository.save(equipmentType);

    List<EquipmentType> equipmentTypes = repository.findByName(
            EQUIPMENT_TYPE_NAME);

    assertEquals(1, equipmentTypes.size());
    assertTrue(equipmentTypes.stream().allMatch(result -> equipmentTypes
            .contains(equipmentType)));
  }

  @Test
  public void shouldFindByCode() {

    equipmentType = generateEquipmentType();
    repository.save(equipmentType);

    List<EquipmentType> equipmentTypes = repository.findByCode(EQUIPMENT_TYPE_CODE);
    assertEquals(1, equipmentTypes.size());
    assertTrue(equipmentTypes.stream().allMatch(result -> equipmentTypes
            .contains(equipmentType)));
  }



  @Test
  public void shouldFindActive() {

    equipmentType = generateEquipmentType();
    repository.save(equipmentType);

    Set<EquipmentType> equipmentTypes = repository.findByActive(true);
    assertEquals(1, equipmentTypes.size());
    assertTrue(equipmentTypes.stream().allMatch(result -> equipmentTypes
            .contains(equipmentType)));
  }


  @Test
  public void shouldReturnAll() {
    equipmentType = generateEquipmentType();
    repository.save(equipmentType);

    Page equipmentTypes = repository.findAllWithoutSnapshots(pageable);

    assertEquals(1, equipmentTypes.getContent().size());
    assertEquals(1, equipmentTypes.getContent().size());
  }

}
