/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.equipment.repository;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;
import java.util.UUID;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;
import org.junit.Before;
import org.junit.Test;
import org.openlmis.equipment.domain.Discipline;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;

public class DisciplineRepositoryIntegrationTest
        extends BaseCrudRepositoryIntegrationTest<Discipline> {

  @Autowired
  private DisciplineRepository repository;

  @Autowired
  private EntityManager entityManager;

  @Override
  CrudRepository<Discipline, UUID> getRepository() {
    return repository;
  }

  @Override
  Discipline generateInstance() {
    return new Discipline("a","b",1,false);
  }

  @Before
  public void initialize() {

  }

  @Test(expected = PersistenceException.class)
  public void shouldNotAllowSavingOfDisciplinesWithSameName() {
    Discipline discipline = new Discipline("a","b",1,false);
    Discipline discipline1 = new Discipline("a","c",2,false);
    repository.save(discipline);
    repository.save(discipline1);
    entityManager.flush();
  }

  @Test(expected = PersistenceException.class)
  public void shouldNotAllowSavingDisciplinesWithSameCode() {
    Discipline discipline = new Discipline("a","b",1,false);
    Discipline discipline1 = new Discipline("c","b",2,false);
    repository.save(discipline);
    repository.save(discipline1);
    entityManager.flush();
  }

  @Test
  public void shouldFindDisciplineByName() {
    Discipline discipline = new Discipline("a","b",1,false);
    repository.save(discipline);
    Discipline discipline1 = new Discipline("aa","c",2,false);
    repository.save(discipline1);
    Discipline discipline2 = new Discipline("aaa","d",3,false);
    repository.save(discipline2);
    List<Discipline> disciplineList = repository.findByName("a");
    assertThat(3).isEqualTo(disciplineList.size());
    assertThat(discipline.getDisplayOrder()).isEqualTo(disciplineList.get(0).getDisplayOrder());
    assertThat(discipline1.getDisplayOrder()).isEqualTo(disciplineList.get(1).getDisplayOrder());
    assertThat(discipline2.getDisplayOrder()).isEqualTo(disciplineList.get(2).getDisplayOrder());
  }

  @Test
  public void shouldFindDisciplineByCode() {
    Discipline discipline = new Discipline("a","b",1,false);
    repository.save(discipline);
    Discipline discipline1 = new Discipline("c","bb",2,false);
    repository.save(discipline1);
    Discipline discipline2 = new Discipline("d","bbb",3,false);
    repository.save(discipline2);
    List<Discipline> disciplineList = repository.findByCode("b");
    assertThat(3).isEqualTo(disciplineList.size());
    assertThat(discipline.getDisplayOrder()).isEqualTo(disciplineList.get(0).getDisplayOrder());
    assertThat(discipline1.getDisplayOrder()).isEqualTo(disciplineList.get(1).getDisplayOrder());
    assertThat(discipline2.getDisplayOrder()).isEqualTo(disciplineList.get(2).getDisplayOrder());
  }
}
