CREATE TABLE equipment_disciplines (
    id uuid NOT NULL,
    name text NOT NULL UNIQUE,
    code text NOT NULL UNIQUE,
    displayOrder integer,
    enabled BOOLEAN NOT NULL
);