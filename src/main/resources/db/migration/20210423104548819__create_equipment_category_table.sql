CREATE TABLE equipment_category
(
    id             uuid    NOT NULL,
    code           text    NOT NULL,
    name           text,
    displayorder integer NOT NULL,
    active         boolean NOT NULL,
    modifieddate   timestamp with time zone,
    lastModifier   uuid
);