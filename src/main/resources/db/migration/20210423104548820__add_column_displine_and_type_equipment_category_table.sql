
ALTER TABLE ONLY equipment_category
    ADD CONSTRAINT equipment_category_pkey PRIMARY KEY (id);

ALTER TABLE ONLY equipment_disciplines
    ADD CONSTRAINT equipment_disciplines_pkey PRIMARY KEY (id);


ALTER TABLE ONLY equipment_category
    ADD COLUMN IF NOT EXISTS equipmenttypeid uuid;

ALTER TABLE ONLY equipment_category
    ADD CONSTRAINT frk_equipment_type_id FOREIGN KEY (equipmenttypeid)
    REFERENCES equipment_types(id);

ALTER TABLE ONLY equipment_category
    ADD COLUMN IF NOT EXISTS disciplineid uuid;

ALTER TABLE ONLY equipment_category
    ADD CONSTRAINT frk_discipline_id FOREIGN KEY (disciplineid)
    REFERENCES equipment_disciplines(id);