CREATE TABLE equipment_operation_modes (
    id uuid NOT NULL,
    name text NOT NULL UNIQUE,
    code text NOT NULL UNIQUE,
    displayorder integer NOT NULL,
    description text,
    active boolean NOT NULL default true
);
