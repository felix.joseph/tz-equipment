CREATE TABLE equipment_types
(
    id             uuid NOT NULL,
    code           text NOT NULL,
    name           text NOT NULL,
    isbiochemistry boolean DEFAULT false,
    iscoldchain    boolean DEFAULT false,
    active         boolean DEFAULT false,
    modifieddate   timestamp with time zone,
    lastModifier   uuid
);

ALTER TABLE ONLY equipment_types
    ADD CONSTRAINT equipment_type_pkey PRIMARY KEY (id);
