/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.equipment.web;

import static org.openlmis.equipment.i18n.MessageKeys.ERROR_NOT_FOUND;
import static org.openlmis.equipment.service.ResourceNames.BASE_PATH;
import static org.openlmis.equipment.web.EquipmentCategoryController.RESOURCE_PATH;

import com.google.common.collect.Sets;

import java.util.Objects;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import lombok.NoArgsConstructor;
import org.openlmis.equipment.domain.EquipmentCategory;
import org.openlmis.equipment.dto.EquipmentCategoryDto;
import org.openlmis.equipment.exception.NotFoundException;
import org.openlmis.equipment.exception.ValidationMessageException;
import org.openlmis.equipment.i18n.MessageKeys;
import org.openlmis.equipment.repository.EquipmentCategoryRepository;
import org.openlmis.equipment.service.PermissionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;


@NoArgsConstructor
@Controller
@Transactional
@RequestMapping(RESOURCE_PATH)
public class EquipmentCategoryController extends BaseController {

  static final String RESOURCE_PATH = BASE_PATH + "/equipmentCategories";

  private static final Logger LOGGER = LoggerFactory.getLogger(EquipmentCategoryController.class);

  @Autowired
  private PermissionService permissionService;

  @Autowired
  private EquipmentCategoryRepository equipmentCategoryRepository;

  public EquipmentCategoryController(EquipmentCategoryRepository repository) {
    this.equipmentCategoryRepository = Objects.requireNonNull(repository);
  }


  /**
   * Get all equipment category in the system.
   *
   * @param pageable pagination params such as sort, offset , pageNumber e.t.c.
   * @return all equipment list in the system.
   */
  @GetMapping()
  @ResponseStatus(HttpStatus.OK)
  @ResponseBody
  public Page<EquipmentCategoryDto> findAll(Pageable pageable) {

    Set<EquipmentCategory> equipmentCategories =
            Sets.newHashSet(equipmentCategoryRepository.findAll());
    Set<EquipmentCategoryDto> equipmentCategoryDtos =
            equipmentCategories.stream().map(this::exportToDto).collect(Collectors.toSet());

    return toPage(equipmentCategoryDtos, pageable);
  }

  /**
   * Save equipment Category using provided equipment category DTO
   * If category does not exist, will create new one and
   * If does exist will update it.
   *
   * @param equipmentCategoryDto provide Equipment Category DTO.
   * @return saved equipment category.
   */
  @PostMapping()
  @ResponseStatus(HttpStatus.CREATED)
  @ResponseBody
  public EquipmentCategoryDto create(
          @RequestBody EquipmentCategoryDto equipmentCategoryDto) {

    permissionService.canManageCce();

    if (equipmentCategoryDto.getCode() == null || equipmentCategoryDto.getName() == null) {
      throw new ValidationMessageException(MessageKeys.ERROR_MISSING_MANDATORY_ITEMS);
    }
    EquipmentCategory equipmentCategoryToSave =
            EquipmentCategory.newEquipmentCategory(equipmentCategoryDto);
    equipmentCategoryToSave = equipmentCategoryRepository.save(equipmentCategoryToSave);
    LOGGER.debug("Saved equipment category with id: " + equipmentCategoryToSave.getId());

    return exportToDto(equipmentCategoryToSave);
  }

  /**
   * Get chosen equipment category.
   *
   * @param id of equipment category to get.
   * @return equipment category.
   */

  @GetMapping(value = "/{id}")
  @ResponseStatus(HttpStatus.OK)
  @ResponseBody
  public EquipmentCategoryDto get(@PathVariable("id") UUID id) {

    EquipmentCategory equipmentCategory =
            equipmentCategoryRepository.findById(id)
                    .orElseThrow(() -> new NotFoundException(ERROR_NOT_FOUND));
    return exportToDto(equipmentCategory);
  }


  /**
   * Updating equipment Type.
   *
   * @param equipmentCategoryDto a EquipmentCategoryDto bound to the request body.
   * @param equipmentCategoryId  the UUID of Equipment Category which we want to update.
   * @return updated equipment type DTO.
   */
  @PutMapping(value = "/{id}")
  @ResponseStatus(HttpStatus.OK)
  @ResponseBody
  public EquipmentCategoryDto update(
          @RequestBody EquipmentCategoryDto equipmentCategoryDto,
          @PathVariable("id") UUID equipmentCategoryId) {

    permissionService.canManageCce();

    if (equipmentCategoryDto.getCode() == null || equipmentCategoryDto.getName() == null) {
      throw new ValidationMessageException(MessageKeys.ERROR_MISSING_MANDATORY_ITEMS);
    }

    EquipmentCategory template = EquipmentCategory.newEquipmentCategory(equipmentCategoryDto);
    EquipmentCategory equipmentCategoryToUpdate =
            equipmentCategoryRepository.findById(equipmentCategoryId).orElse(null);

    EquipmentCategory equipmentCategoryToSave;
    if (equipmentCategoryToUpdate == null) {
      equipmentCategoryToSave = template;
      equipmentCategoryToSave.setId(equipmentCategoryId);
    } else {
      equipmentCategoryToSave = equipmentCategoryToUpdate;
      equipmentCategoryToSave.updateFrom(template);
    }

    equipmentCategoryToSave = equipmentCategoryRepository.save(equipmentCategoryToSave);

    return exportToDto(equipmentCategoryToSave);
  }

  private EquipmentCategoryDto exportToDto(EquipmentCategory equipmentCategory) {
    EquipmentCategoryDto equipmentCategoryDto = new EquipmentCategoryDto();
    equipmentCategory.export(equipmentCategoryDto);
    return equipmentCategoryDto;
  }
}
