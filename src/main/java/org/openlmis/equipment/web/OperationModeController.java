/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.equipment.web;

import static org.openlmis.equipment.i18n.OperationModeMessageKeys.ERROR_OPERATION_MODE_NOT_FOUND;
import static org.openlmis.equipment.service.ResourceNames.BASE_PATH;
import static org.openlmis.equipment.web.OperationModeController.RESOURCE_PATH;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import javax.transaction.Transactional;

import org.openlmis.equipment.domain.OperationMode;
import org.openlmis.equipment.dto.OperationModeDto;
import org.openlmis.equipment.exception.NotFoundException;
import org.openlmis.equipment.repository.OperationModeRepository;
import org.openlmis.equipment.service.PermissionService;
import org.openlmis.equipment.util.Pagination;
import org.openlmis.equipment.web.validator.OperationModeValidator;
import org.slf4j.ext.XLogger;
import org.slf4j.ext.XLoggerFactory;
import org.slf4j.profiler.Profiler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.SortDefault;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Transactional
@RequestMapping (RESOURCE_PATH)
public class OperationModeController extends BaseController {

  private static final XLogger XLOGGER = XLoggerFactory.getXLogger(OperationModeController.class);

  static final String RESOURCE_PATH = BASE_PATH + "/equipmentOperationModes";

  private static final String PROFILER_CHECK_PERMISSION = "CHECK_PERMISSION";

  @Autowired
  private OperationModeRepository operationModeRepository;

  @Autowired
  private OperationModeDtoBuilder operationModeDtoBuilder;

  @Autowired
  private OperationModeValidator operationModeValidator;

  @Autowired
  private PermissionService permissionService;

  /**
   * Allows creating new OperationMode.
   *
   * @param operationModeDto bound to the request body.
   * @return created operationMode.
   */
  @PostMapping ()
  @ResponseStatus (HttpStatus.CREATED)
  public OperationModeDto create(@RequestBody OperationModeDto operationModeDto) {
    XLOGGER.entry(operationModeDto);
    Profiler profiler = new Profiler("CREATE_OPERATION_MODE");
    profiler.setLogger(XLOGGER);

    profiler.start(PROFILER_CHECK_PERMISSION);
    permissionService.canManageCce();

    profiler.start("VALIDATE");
    operationModeValidator.validateNewOperationMode(operationModeDto);

    profiler.start("CREATE_DOMAIN_INSTANCE");
    OperationMode operationMode = OperationMode.newInstance(operationModeDto);

    profiler.start("SAVE_OPERATION_MODE");
    OperationMode newOperationMode = operationModeRepository.save(operationMode);

    profiler.start("CREATE_DTO");
    OperationModeDto dto = operationModeDtoBuilder.build(newOperationMode);

    profiler.stop().log();
    XLOGGER.exit(dto);

    return dto;
  }

  /**
   * Get all OperationModes.
   *
   * @return OperationModes.
   */
  @GetMapping ()
  @ResponseStatus (HttpStatus.OK)
  public Page<OperationModeDto> findAll(@SortDefault (sort = "name") Pageable pageable) {
    XLOGGER.entry(pageable);
    Profiler profiler = new Profiler("GET_OPERATION_MODES");
    profiler.setLogger(XLOGGER);

    profiler.start("GET_FROM_DB");
    Page<OperationMode> operationModesPage = operationModeRepository.findAll(pageable);

    profiler.start("CREATE_DTOS");
    List<OperationModeDto> dtos = operationModeDtoBuilder.build(
        operationModesPage.getContent()
    );

    profiler.start("CREATE_PAGE");
    Page<OperationModeDto> page = Pagination.getPage(
        dtos, pageable, operationModesPage.getTotalElements()
    );

    profiler.stop().log();
    XLOGGER.exit(page);

    return page;
  }

  /**
   * Allows updating existing operationMode.
   *
   * @param operationModeDto bound to the request body.
   * @return updated operationMode.
   */
  @PutMapping ("/{id}")
  @ResponseStatus (HttpStatus.OK)
  public OperationModeDto updateOperationMode(@PathVariable ("id") UUID id,
                                              @RequestBody OperationModeDto operationModeDto) {
    XLOGGER.entry(id);
    Profiler profiler = new Profiler("UPDATE_OPERATION_MODE");
    profiler.setLogger(XLOGGER);

    profiler.start(PROFILER_CHECK_PERMISSION);
    permissionService.canManageCce();

    profiler.start("FIND_IN_DB");
    Optional<OperationMode> optional = operationModeRepository.findById(id);

    profiler.start("VALIDATE_AND_CREATE_DOMAIN_INSTANCE");
    OperationMode operationMode;
    if (optional.isPresent()) {
      operationModeValidator.validateExistingOperationMode(operationModeDto, id);
      operationMode = optional.get();
      operationMode.setCode(operationModeDto.getCode());
      operationMode.setName(operationModeDto.getName());
      operationMode.setDisplayOrder(operationModeDto.getDisplayOrder());
      operationMode.setDescription(operationModeDto.getDescription());
      operationMode.setActive(operationModeDto.getActive());
    } else {
      operationModeValidator.validateNewOperationMode(operationModeDto);
      operationMode = OperationMode.newInstance(operationModeDto);
    }

    profiler.start("SAVE_OPERATION_MODE");
    OperationMode newOperationMode = operationModeRepository.save(operationMode);

    profiler.start("CREATE_DTO");
    OperationModeDto dto = operationModeDtoBuilder.build(newOperationMode);

    profiler.stop().log();
    XLOGGER.exit(dto);

    return dto;
  }

  /**
   * should return operationMode with the provided id if it exists.
   *
   * @param id uuid passed as path variable.
   * @return updated operationModeDto.
   */
  @GetMapping ("/{id}")
  @ResponseStatus (HttpStatus.OK)
  public OperationModeDto getOperationMode(@PathVariable ("id") UUID id) {
    XLOGGER.entry(id);
    Profiler profiler = new Profiler("GET_OPERATION_MODE_BY_ID");
    profiler.setLogger(XLOGGER);

    profiler.start("FIND_IN_DB");
    Optional<OperationMode> optional = operationModeRepository.findById(id);
    if (!optional.isPresent()) {
      profiler.stop().log();
      XLOGGER.exit(id);

      throw new NotFoundException(ERROR_OPERATION_MODE_NOT_FOUND);
    }

    profiler.start("PROFILER_CREATE_DTO");
    OperationModeDto operationModeDto = operationModeDtoBuilder.build(optional.get());

    profiler.stop().log();
    XLOGGER.exit(operationModeDto);

    return operationModeDto;
  }

}
