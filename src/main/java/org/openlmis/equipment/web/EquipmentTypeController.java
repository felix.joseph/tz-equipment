/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.equipment.web;

import static org.openlmis.equipment.i18n.MessageKeys.ERROR_NOT_FOUND;
import static org.openlmis.equipment.service.ResourceNames.BASE_PATH;
import static org.openlmis.equipment.web.EquipmentTypeController.RESOURCE_PATH;

import com.google.common.collect.Sets;

import java.util.Objects;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import lombok.NoArgsConstructor;
import org.openlmis.equipment.domain.EquipmentType;
import org.openlmis.equipment.dto.EquipmentTypeDto;
import org.openlmis.equipment.exception.NotFoundException;
import org.openlmis.equipment.exception.ValidationMessageException;
import org.openlmis.equipment.i18n.MessageKeys;
import org.openlmis.equipment.repository.EquipmentTypeRepository;
import org.openlmis.equipment.service.PermissionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@NoArgsConstructor
@Controller
@Transactional
@RequestMapping(RESOURCE_PATH)
public class EquipmentTypeController extends BaseController {

  static final String RESOURCE_PATH = BASE_PATH + "/equipmentTypes";

  private static final Logger LOGGER = LoggerFactory.getLogger(EquipmentTypeController.class);

  @Autowired
  private EquipmentTypeRepository equipmentTypeRepository;

  @Autowired
  private PermissionService permissionService;

  public EquipmentTypeController(EquipmentTypeRepository repository) {
    this.equipmentTypeRepository = Objects.requireNonNull(repository);
  }

  /**
   * Get all equipment types in the system.
   *
   * @return all equipment types in the system.
   */
  @GetMapping()
  @ResponseStatus(HttpStatus.OK)
  @ResponseBody
  public Page<EquipmentTypeDto> findAll(Pageable pageable) {

    Set<EquipmentType> equipmentTypes = Sets.newHashSet(equipmentTypeRepository.findAll());
    Set<EquipmentTypeDto> equipmentTypeDtos =
            equipmentTypes.stream().map(this::exportToDto).collect(Collectors.toSet());

    return toPage(equipmentTypeDtos, pageable);
  }

  /**
   * Get chosen equipment type.
   *
   * @param id id of the equipment type to get.
   * @return the equipment type.
   */
  @GetMapping(value = "/{id}")
  @ResponseStatus(HttpStatus.OK)
  @ResponseBody
  public EquipmentTypeDto get(@PathVariable("id") UUID id) {

    EquipmentType equipmentType =
            equipmentTypeRepository.findById(id)
                    .orElseThrow(() -> new NotFoundException(ERROR_NOT_FOUND));
    return exportToDto(equipmentType);
  }

  /**
   * Save a equipment type using the provided equipment type DTO
   * If the type does not exist, will create one. If it
   * does exist, will update it.
   *
   * @param equipmentTypeDto provided equipment type DTO.
   * @return the saved equipment type.
   */
  @PostMapping()
  @ResponseStatus(HttpStatus.CREATED)
  @ResponseBody
  public EquipmentTypeDto create(
          @RequestBody EquipmentTypeDto equipmentTypeDto) {

    permissionService.canManageCce();

    if (equipmentTypeDto.getCode() == null || equipmentTypeDto.getName() == null) {
      throw new ValidationMessageException(MessageKeys.ERROR_MISSING_MANDATORY_ITEMS);
    }
    EquipmentType equipmentTypeToSave = EquipmentType.newEquipmentType(equipmentTypeDto);
    equipmentTypeToSave = equipmentTypeRepository.save(equipmentTypeToSave);
    LOGGER.debug("Saved equipment type with id: " + equipmentTypeToSave.getId());

    return exportToDto(equipmentTypeToSave);
  }

  /**
   * Updating Equipment Type.
   *
   * @param equipmentTypeDto a EquipmentTypeDto bound to the request body.
   * @param equipmentTypeId  the UUID of Equipment Type which we want to update.
   * @return the updated EquipmentTypeDto.
   */
  @PutMapping(value = "/{id}")
  @ResponseStatus(HttpStatus.OK)
  @ResponseBody
  public EquipmentTypeDto update(
          @RequestBody EquipmentTypeDto equipmentTypeDto,
          @PathVariable("id") UUID equipmentTypeId) {

    permissionService.canManageCce();

    if (equipmentTypeDto.getCode() == null || equipmentTypeDto.getName() == null) {
      throw new ValidationMessageException(MessageKeys.ERROR_MISSING_MANDATORY_ITEMS);
    }

    EquipmentType template = EquipmentType.newEquipmentType(equipmentTypeDto);
    EquipmentType equipmentTypeToUpdate =
            equipmentTypeRepository.findById(equipmentTypeId).orElse(null);

    EquipmentType equipmentTypeToSave;
    if (equipmentTypeToUpdate == null) {
      equipmentTypeToSave = template;
      equipmentTypeToSave.setId(equipmentTypeId);
    } else {
      equipmentTypeToSave = equipmentTypeToUpdate;
      equipmentTypeToSave.updateFrom(template);
    }

    equipmentTypeToSave = equipmentTypeRepository.save(equipmentTypeToSave);

    return exportToDto(equipmentTypeToSave);
  }

  private EquipmentTypeDto exportToDto(EquipmentType equipmentType) {
    EquipmentTypeDto equipmentTypeDto = new EquipmentTypeDto();
    equipmentType.export(equipmentTypeDto);
    return equipmentTypeDto;
  }

}
