/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */


package org.openlmis.equipment.web;

import java.util.ArrayList;
import java.util.List;

import org.openlmis.equipment.domain.OperationMode;
import org.openlmis.equipment.dto.OperationModeDto;
import org.springframework.stereotype.Component;

@Component
public class OperationModeDtoBuilder {
  /**
   * Create a list of {@link OperationModeDto} based on passed data.
   *
   * @param operationModes a list of inventory items that will be converted into DTOs.
   * @return a list of {@link OperationModeDto}
   */
  public List<OperationModeDto> build(Iterable<OperationMode> operationModes) {

    List<OperationModeDto> builtOperationModes = new ArrayList<>();

    for (OperationMode operationMode : operationModes) {
      if (operationMode == null) {
        continue;
      }
      OperationModeDto dto = export(operationMode);
      builtOperationModes.add(dto);
    }
    return builtOperationModes;
  }

  /**
   * Create a new instance of {@link OperationModeDto} based on data from {@link OperationMode}.
   *
   * @param operationMode instance used to create {@link OperationModeDto} (can be {@code null})
   * @return new instance of {@link OperationModeDto}. {@code null} if passed argument is {@code
   * null}.
   */
  public OperationModeDto build(OperationMode operationMode) {
    if (operationMode == null) {
      return null;
    }

    return export(operationMode);
  }

  private OperationModeDto export(OperationMode operationMode) {
    OperationModeDto dto = new OperationModeDto();
    operationMode.export(dto);
    return dto;
  }

}
