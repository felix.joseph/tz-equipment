/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.equipment.web;

import static org.openlmis.equipment.i18n.DisciplineMessageKeys.ERROR_DISCIPLINE_NOT_FOUND;
import static org.openlmis.equipment.service.ResourceNames.BASE_PATH;
import static org.openlmis.equipment.web.DisciplineController.RESOURCE_PATH;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.openlmis.equipment.domain.Discipline;
import org.openlmis.equipment.dto.DisciplineDto;
import org.openlmis.equipment.exception.NotFoundException;
import org.openlmis.equipment.exception.ValidationMessageException;
import org.openlmis.equipment.i18n.DisciplineMessageKeys;
import org.openlmis.equipment.repository.DisciplineRepository;
import org.openlmis.equipment.service.PermissionService;
import org.openlmis.equipment.util.Message;
import org.openlmis.equipment.util.Pagination;
import org.openlmis.equipment.web.validator.DisciplineValidator;
import org.slf4j.ext.XLogger;
import org.slf4j.ext.XLoggerFactory;
import org.slf4j.profiler.Profiler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.SortDefault;
import org.springframework.http.HttpStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Transactional
@RequestMapping(RESOURCE_PATH)
public class DisciplineController extends BaseController {
  private static final XLogger XLOGGER = XLoggerFactory.getXLogger(DisciplineController.class);
  private static final String PROFILER_CHECK_PERMISSION = "CHECK_PERMISSION";

  static final String RESOURCE_PATH = BASE_PATH + "/disciplines";

  @Autowired
  private DisciplineRepository disciplineRepository;

  @Autowired
  private DisciplineDtoBuilder disciplineDtoBuilder;

  @Autowired
  private DisciplineValidator validator;

  @Autowired
  private PermissionService permissionService;

  /**
   * Allows creating new Discipline.
   *
   * @param disciplineDto bound to the request body.
   * @return created discipline.
   */
  @PostMapping()
  @ResponseStatus(HttpStatus.CREATED)
  public DisciplineDto create(@RequestBody DisciplineDto disciplineDto,
                              BindingResult bindingResult) {
    XLOGGER.entry(disciplineDto);
    Profiler profiler = new Profiler("CREATE_DISCIPLINE");
    profiler.setLogger(XLOGGER);
    profiler.start(PROFILER_CHECK_PERMISSION);
    permissionService.canManageCce();

    profiler.start("VALIDATE");
    List<Discipline> disciplines = disciplineRepository.findByName(disciplineDto.getName());
    if (disciplines.size() > 0) {
      throw new ValidationMessageException(DisciplineMessageKeys.ERROR_DISCIPLINE_NAME_EXISTS);
    }

    List<Discipline> disciplineList = disciplineRepository.findByCode(disciplineDto.getCode());
    if (disciplineList.size() > 0) {
      throw new ValidationMessageException(DisciplineMessageKeys.ERROR_DISCIPLINE_CODE_EXISTS);
    }

    validator.validate(disciplineDto,bindingResult);
    throwValidationMessageExceptionIfErrors(bindingResult);

    profiler.start("CREATE_DISCIPLINE_DOMAIN");
    Discipline discipline = Discipline.newInstance(disciplineDto);

    profiler.start("SAVE_DISCIPLINE_AND_CREATE_DTO");
    DisciplineDto dto = disciplineDtoBuilder.build(disciplineRepository.save(discipline));

    profiler.stop().log();
    XLOGGER.exit(dto);
    return dto;
  }

  /**
   * Get all Disciplines.
   *
   * @return Disciplines.
   */
  @GetMapping()
  @ResponseStatus(HttpStatus.OK)
  public Page<DisciplineDto> findAll(@SortDefault(sort = "name") Pageable pageable) {
    XLOGGER.entry(pageable);
    Profiler profiler = new Profiler("FETCH_DISCIPLINES");
    profiler.setLogger(XLOGGER);
    profiler.start("FETCH_ALL_DISCIPLINES");
    Page<Discipline> disciplinesPage = disciplineRepository.findAll(pageable);

    profiler.start("BUILD_DISCIPLINE_DTO_LIST");
    List<DisciplineDto> dtos = disciplineDtoBuilder.build(disciplinesPage.getContent());

    profiler.start("BUILD_DISCIPLINE_PAGES");
    Page<DisciplineDto> page = Pagination.getPage(dtos, pageable,
            disciplinesPage.getTotalElements());

    profiler.stop().log();
    XLOGGER.exit(page);
    return page;
  }

  /**
   * Allows updating existing discipline.
   *
   * @param disciplineDto bound to the request body.
   * @return updated discipline.
   */
  @PutMapping(value = "/{id}")
  @ResponseStatus(HttpStatus.OK)
  public DisciplineDto updateDiscipline(@PathVariable("id") UUID id,
                                        @RequestBody DisciplineDto disciplineDto,
                                        BindingResult bindingResult) {
    XLOGGER.entry(id,disciplineDto);
    Profiler profiler = new Profiler("UPDATE_DISCIPLINE");
    profiler.setLogger(XLOGGER);
    profiler.start(PROFILER_CHECK_PERMISSION);
    permissionService.canManageCce();

    profiler.start("FETCH_DISCIPLINE_BY_ID");
    Optional<Discipline> optional = disciplineRepository.findById(id);
    Discipline discipline;
    if (optional.isPresent()) {
      discipline = optional.get();
      discipline.setCode(disciplineDto.getCode());
      discipline.setName(disciplineDto.getName());
      discipline.setDisplayOrder(disciplineDto.getDisplayOrder());
      discipline.setEnabled(disciplineDto.getEnabled());
    } else {
      profiler.start("VALIDATE DISCIPLINE");
      validator.validate(disciplineDto,bindingResult);
      throwValidationMessageExceptionIfErrors(bindingResult);
      discipline = Discipline.newInstance(disciplineDto);
    }

    profiler.start("UPDATE_DISCIPLINE_CREATE_DTO");
    DisciplineDto dto = disciplineDtoBuilder.build(disciplineRepository.save(discipline));

    profiler.stop().log();
    XLOGGER.exit(dto);
    return dto;
  }

  /**
   * should return discipline with the provided id if it exists.
   * @param id uuid passed as path variable.
   * @return updated disciplineDto.
   */
  @GetMapping("/{id}")
  @ResponseStatus(HttpStatus.OK)
  public DisciplineDto getDiscipline(@PathVariable("id") UUID id) {
    XLOGGER.entry(id);
    Profiler profiler = new Profiler("GET_DISCIPLINE_WITH_ID");
    profiler.setLogger(XLOGGER);
    profiler.start("FETCH_DISCIPLINE_BY_ID");
    Optional<Discipline> optional = disciplineRepository.findById(id);
    if (optional.isPresent()) {
      DisciplineDto disciplineDto = disciplineDtoBuilder.build(optional.get());

      profiler.stop().log();
      XLOGGER.exit(disciplineDto);
      return disciplineDto;
    } else {
      profiler.stop().log();
      XLOGGER.exit(id);

      throw new NotFoundException(ERROR_DISCIPLINE_NOT_FOUND);
    }
  }

  /**
   * throws exception in case of validation errors.
   * @param bindingResult error wrapper
   */
  public void throwValidationMessageExceptionIfErrors(BindingResult bindingResult) {
    if (bindingResult.getErrorCount() > 0) {
      throw new ValidationMessageException(new Message(bindingResult.getFieldError().getCode(),
              bindingResult.getFieldError().getArguments()));
    }
  }
}
