/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.equipment.web.validator;

import static org.openlmis.equipment.i18n.OperationModeMessageKeys.ERROR_OPERATION_MODE_ACTIVE_FIELD_IS_REQUIRED;
import static org.openlmis.equipment.i18n.OperationModeMessageKeys.ERROR_OPERATION_MODE_CODE_IS_REQUIRED;
import static org.openlmis.equipment.i18n.OperationModeMessageKeys.ERROR_OPERATION_MODE_CODE_NOT_UNIQUE;
import static org.openlmis.equipment.i18n.OperationModeMessageKeys.ERROR_OPERATION_MODE_DISPLAY_ORDER_IS_REQUIRED;
import static org.openlmis.equipment.i18n.OperationModeMessageKeys.ERROR_OPERATION_MODE_NAME_IS_REQUIRED;
import static org.openlmis.equipment.i18n.OperationModeMessageKeys.ERROR_OPERATION_MODE_NAME_NOT_UNIQUE;

import java.util.UUID;
import org.openlmis.equipment.dto.OperationModeDto;
import org.openlmis.equipment.exception.ValidationMessageException;
import org.openlmis.equipment.repository.OperationModeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


@Component
public class OperationModeValidator {

  @Autowired
  private OperationModeRepository operationModeRepository;

  public void validateNewOperationMode(OperationModeDto operationModeDto) {
    validateEmptyFieldValues(operationModeDto);
    validateUniqueConstraints(operationModeDto);
  }

  public void validateExistingOperationMode(OperationModeDto operationModeDto, UUID id) {
    validateEmptyFieldValues(operationModeDto);
    validateUpdatedUniqueFields(operationModeDto, id);
  }

  /**
   * method to validate and reject non unique updated values.
   *
   * @param operationModeDto dto object to validate
   */
  public void validateUpdatedUniqueFields(OperationModeDto operationModeDto, UUID id) {
    if (operationModeRepository.existsByCodeAndIdNot(operationModeDto.getCode(), id)) {
      throw new ValidationMessageException(
          ERROR_OPERATION_MODE_CODE_NOT_UNIQUE, operationModeDto.getCode()
      );
    }

    if (operationModeRepository.existsByNameAndIdNot(operationModeDto.getName(), id)) {
      throw new ValidationMessageException(
          ERROR_OPERATION_MODE_NAME_NOT_UNIQUE, operationModeDto.getName()
      );
    }
  }

  /**
   * method to validate and reject non unique values.
   *
   * @param operationModeDto dto object to validate
   */
  public void validateUniqueConstraints(OperationModeDto operationModeDto) {
    if (
        operationModeDto.getCode() != null
            && operationModeRepository.existsByCode(operationModeDto.getCode())
    ) {
      throw new ValidationMessageException(
          ERROR_OPERATION_MODE_CODE_NOT_UNIQUE, operationModeDto.getCode()
      );
    }
    if (operationModeDto.getName() != null
        && operationModeRepository.existsByName(operationModeDto.getName())
    ) {
      throw new ValidationMessageException(
          ERROR_OPERATION_MODE_NAME_NOT_UNIQUE, operationModeDto.getName()
      );
    }
  }

  /**
   * method to validate and reject empty values.
   *
   * @param operationModeDto dto object to validate
   */
  public void validateEmptyFieldValues(OperationModeDto operationModeDto) {
    if (operationModeDto.getCode() == null
        || operationModeDto.getCode().trim().length() == 0) {
      throw new ValidationMessageException(ERROR_OPERATION_MODE_CODE_IS_REQUIRED);
    }
    if (operationModeDto.getName() == null
        || operationModeDto.getName().trim().length() == 0) {
      throw new ValidationMessageException(ERROR_OPERATION_MODE_NAME_IS_REQUIRED);
    }
    if (operationModeDto.getActive() == null) {
      throw new ValidationMessageException(ERROR_OPERATION_MODE_ACTIVE_FIELD_IS_REQUIRED);
    }
    if (operationModeDto.getDisplayOrder() == null) {
      throw new ValidationMessageException(ERROR_OPERATION_MODE_DISPLAY_ORDER_IS_REQUIRED);
    }
  }
}
