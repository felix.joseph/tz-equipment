/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.equipment.web.validator;

import org.openlmis.equipment.dto.DisciplineDto;
import org.openlmis.equipment.i18n.DisciplineMessageKeys;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
public class DisciplineValidator implements Validator {

  @Override
  public boolean supports(Class<?> clazz) {
    return DisciplineDto.class.equals(clazz);
  }

  @Override
  public void validate(Object target, Errors errors) {
    rejectIfNull(errors,"name",DisciplineMessageKeys.NAME_IS_REQUIRED);
    rejectIfNull(errors,"code",DisciplineMessageKeys.CODE_IS_REQUIRED);
    rejectIfNull(errors,"enabled",DisciplineMessageKeys.ENABLED_IS_REQUIRED);
  }

  /**
   * method to reject null values.
   *
   * @param errors thrown
   * @param field property to check for null value
   * @param message to be displayed to end user
   */
  public void rejectIfNull(Errors errors, String field, String message) {
    if (errors.getFieldValue(field) == null) {
      errors.rejectValue(field, message, message);
    }
  }
}
