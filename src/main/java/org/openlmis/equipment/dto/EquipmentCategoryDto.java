/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.equipment.dto;

import java.util.Objects;

import lombok.Getter;
import lombok.Setter;

import org.openlmis.equipment.domain.Discipline;
import org.openlmis.equipment.domain.EquipmentCategory;
import org.openlmis.equipment.domain.EquipmentType;


public class EquipmentCategoryDto extends BaseDto implements EquipmentCategory.Exporter,
        EquipmentCategory.Importer {
  @Getter
  @Setter
  private String name;

  @Getter
  @Setter
  private String code;

  @Getter
  @Setter
  private Integer displayOrder;

  @Getter
  @Setter
  private Boolean active;


  @Getter
  @Setter
  private EquipmentType equipmentType;


  @Getter
  @Setter
  private Discipline discipline;


  @Override
  public int hashCode() {
    return name.hashCode();
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (!(obj instanceof EquipmentCategoryDto)) {
      return false;
    }
    EquipmentCategoryDto equipmentCategoryDto = (EquipmentCategoryDto) obj;
    return Objects.equals(name, equipmentCategoryDto.name);
  }


}
