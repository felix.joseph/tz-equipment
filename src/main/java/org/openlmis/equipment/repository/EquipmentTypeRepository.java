/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.equipment.repository;

import java.util.List;
import java.util.Set;
import java.util.UUID;

import org.openlmis.equipment.domain.EquipmentType;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

public interface EquipmentTypeRepository extends
        PagingAndSortingRepository<EquipmentType, UUID> {

  @Query("SELECT et FROM EquipmentType et  WHERE et.name LIKE :name% ")
  List<EquipmentType> findByName(@Param("name") String name);

  @Query("SELECT et FROM EquipmentType et WHERE et.code LIKE :code% ")
  List<EquipmentType> findByCode(@Param("code") String code);

  Set<EquipmentType> findByActive(boolean active);

  @Query(value = "SELECT\n"
          + "    r.*\n"
          + "FROM\n"
          + "    equipment.equipment_types r\n"
          + " ",
          nativeQuery = true)
  Page<EquipmentType> findAllWithoutSnapshots(Pageable pageable);
}
