/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.equipment.domain;

import java.util.Objects;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.javers.core.metamodel.annotation.TypeName;


@Entity
@Table(name = "equipment_category")
@NoArgsConstructor
@TypeName("EquipmentCategory")
public class EquipmentCategory extends BaseEntity {
  private static final String TEXT = "text";
  @Column(nullable = false)
  @Getter
  @Setter
  Integer displayOrder;
  @Column(nullable = false, unique = true, columnDefinition = TEXT)
  @Getter
  @Setter
  private String name;
  @Column(nullable = false, unique = true, columnDefinition = TEXT)
  @Getter
  @Setter
  private String code;
  @Getter
  @Setter
  private Boolean active;


  @ManyToOne
  @JoinColumn(name = "equipmenttypeid", nullable = false)
  @Getter
  @Setter
  private EquipmentType equipmentType;

  @ManyToOne
  @JoinColumn(name = "disciplineid", nullable = false)
  @Getter
  @Setter
  private Discipline discipline;

  /**
   *  Equipment Type constructor.
   *
   * @param name of Equipment Category
   * @param code unique code of Equipment Category
   * @param displayOrder display order
   * @param active  active
   * @param equipmentType Equipment type
   * @param discipline discipline from Discipline Domain
   */
  public EquipmentCategory(
          String name, String code, Integer displayOrder, Boolean active,
          EquipmentType equipmentType, Discipline discipline) {
    this.name = name;
    this.code = code;
    this.displayOrder = displayOrder;
    this.active = active;
    this.discipline = discipline;
    this.equipmentType = equipmentType;
  }

  /**
   * Static factory method for constructing a new equipment category.
   *
   * @param name         name
   * @param code         code
   * @param displayOrder displayOrder
   * @param active       active
   * @return new equipment category
   */

  public static EquipmentCategory newEquipmentCategory(
          String name, String code, Integer displayOrder, Boolean active,
          EquipmentType equipmentType, Discipline discipline) {
    return new EquipmentCategory(name, code, displayOrder, active, equipmentType, discipline);
  }

  /**
   * static factory method for constructing a new equipment category using an importer (DTO).
   *
   * @param importer the equipment category importer (DTO)
   * @return new equipment category
   */
  public static EquipmentCategory newEquipmentCategory(Importer importer) {
    EquipmentCategory newEquipmentCategory = new EquipmentCategory(importer.getName(),
            importer.getCode(), importer.getDisplayOrder(),
            importer.getActive(), importer.getEquipmentType(), importer.getDiscipline());
    newEquipmentCategory.id = importer.getId();
    newEquipmentCategory.code = importer.getCode();
    newEquipmentCategory.displayOrder = importer.getDisplayOrder();
    newEquipmentCategory.active = importer.getActive();
    newEquipmentCategory.discipline = importer.getDiscipline();
    newEquipmentCategory.equipmentType = importer.getEquipmentType();
    return newEquipmentCategory;
  }

  @Override
  public int hashCode() {
    return name.hashCode();
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (!(obj instanceof EquipmentCategory)) {
      return false;
    }
    EquipmentCategory equipmentCategory = (EquipmentCategory) obj;
    return Objects.equals(name, equipmentCategory.name);
  }

  /**
   * Export this object to the specified exporter (DTO).
   *
   * @param exporter exporter to export to
   */
  public void export(Exporter exporter) {
    exporter.setId(id);
    exporter.setName(name);
    exporter.setCode(code);
    exporter.setDisplayOrder(displayOrder);
    exporter.setActive(active);
  }

  /**
   * Static factory method for constructing a new right with a name and type.
   *
   * @param equipmentCategory equipment category
   */

  public void updateFrom(EquipmentCategory equipmentCategory) {
    this.name = equipmentCategory.getName();
    this.code = equipmentCategory.getCode();
    this.active = equipmentCategory.getActive();
    this.displayOrder = equipmentCategory.getDisplayOrder();
  }

  public interface Exporter {
    void setId(java.util.UUID id);

    void setName(String name);

    void setCode(String code);

    void setDisplayOrder(Integer displayOrder);

    void setActive(Boolean active);


  }

  public interface Importer {
    UUID getId();

    String getName();

    String getCode();

    Integer getDisplayOrder();

    Boolean getActive();

    Discipline getDiscipline();

    EquipmentType getEquipmentType();

  }

}
