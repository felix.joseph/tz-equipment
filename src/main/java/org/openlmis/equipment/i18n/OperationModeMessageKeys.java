/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */


package org.openlmis.equipment.i18n;

public class OperationModeMessageKeys extends MessageKeys {

  private static final String ERROR_PREFIX = SERVICE_ERROR_PREFIX + ".operationMode";

  public static final String ERROR_OPERATION_MODE_NOT_FOUND = ERROR_PREFIX + ".notFound";

  public static final String ERROR_OPERATION_MODE_NAME_IS_REQUIRED = join(
      ERROR_PREFIX, "name", REQUIRED
  );

  public static final String ERROR_OPERATION_MODE_CODE_IS_REQUIRED = join(
      ERROR_PREFIX, "code", REQUIRED
  );

  public static final String ERROR_OPERATION_MODE_NAME_NOT_UNIQUE = join(
      ERROR_PREFIX, "name", NOT_UNIQUE
  );

  public static final String ERROR_OPERATION_MODE_CODE_NOT_UNIQUE = join(
      ERROR_PREFIX, "code", NOT_UNIQUE
  );

  public static final String ERROR_OPERATION_MODE_ACTIVE_FIELD_IS_REQUIRED = join(
      ERROR_PREFIX, "active", REQUIRED
  );

  public static final String ERROR_OPERATION_MODE_DISPLAY_ORDER_IS_REQUIRED = join(
      ERROR_PREFIX, "displayOrder", REQUIRED
  );
}
