/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.equipment.web.validator;

import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

import java.util.UUID;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.openlmis.equipment.dto.OperationModeDto;
import org.openlmis.equipment.exception.ValidationMessageException;
import org.openlmis.equipment.i18n.OperationModeMessageKeys;
import org.openlmis.equipment.repository.OperationModeRepository;
import org.openlmis.equipment.util.Message;

@RunWith (MockitoJUnitRunner.class)
@SuppressWarnings({"PMD.TooManyMethods"})
public class OperationModeValidatorTest {

  @Rule
  public final ExpectedException expectedEx = ExpectedException.none();

  @InjectMocks
  private OperationModeValidator operationModeValidator;

  @Mock
  private OperationModeRepository operationModeRepository;

  private OperationModeDto operationModeDto;

  @Before
  public void setUp() {
    initMocks(this);

    operationModeDto = new OperationModeDto(
        "Freezer", "freezer", 1, "A new freezer", true);
  }

  @Test
  public void shouldNotThrowExceptionIfRequiredFieldsAreNotNull() {
    operationModeValidator.validateNewOperationMode(operationModeDto);
    operationModeValidator.validateExistingOperationMode(operationModeDto, UUID.randomUUID());
  }

  @Test
  public void shouldThrowExceptionIfNameIsNull() {
    expectedEx.expect(ValidationMessageException.class);
    expectedEx.expectMessage(
        new Message(OperationModeMessageKeys.ERROR_OPERATION_MODE_NAME_IS_REQUIRED, "").toString());

    operationModeDto.setName(null);

    operationModeValidator.validateEmptyFieldValues(operationModeDto);
  }

  @Test
  public void shouldThrowExceptionIfCodeIsNull() {
    expectedEx.expect(ValidationMessageException.class);
    expectedEx.expectMessage(
        new Message(OperationModeMessageKeys.ERROR_OPERATION_MODE_CODE_IS_REQUIRED, "").toString());

    operationModeDto.setCode(null);

    operationModeValidator.validateEmptyFieldValues(operationModeDto);
  }

  @Test
  public void shouldThrowExceptionIfDisplayOrderIsNull() {
    expectedEx.expect(ValidationMessageException.class);
    expectedEx.expectMessage(
        new Message(OperationModeMessageKeys.ERROR_OPERATION_MODE_DISPLAY_ORDER_IS_REQUIRED, "")
            .toString());

    operationModeDto.setDisplayOrder(null);

    operationModeValidator.validateEmptyFieldValues(operationModeDto);
  }

  @Test
  public void shouldThrowExceptionIfActiveIsNull() {
    expectedEx.expect(ValidationMessageException.class);
    expectedEx.expectMessage(
        new Message(OperationModeMessageKeys.ERROR_OPERATION_MODE_ACTIVE_FIELD_IS_REQUIRED, "")
            .toString());

    operationModeDto.setActive(null);

    operationModeValidator.validateEmptyFieldValues(operationModeDto);
  }

  @Test
  public void shouldThrowExceptionIfNameIsEmpty() {
    expectedEx.expect(ValidationMessageException.class);
    expectedEx.expectMessage(
        new Message(OperationModeMessageKeys.ERROR_OPERATION_MODE_NAME_IS_REQUIRED, "").toString());

    operationModeDto.setName("   ");

    operationModeValidator.validateEmptyFieldValues(operationModeDto);
  }

  @Test
  public void shouldThrowExceptionIfCodeIsEmpty() {
    expectedEx.expect(ValidationMessageException.class);
    expectedEx.expectMessage(
        new Message(OperationModeMessageKeys.ERROR_OPERATION_MODE_CODE_IS_REQUIRED, "").toString());

    operationModeDto.setCode("   ");

    operationModeValidator.validateEmptyFieldValues(operationModeDto);
  }

  @Test
  public void shouldThrowExceptionIfOperationModeWithGivenNameExists() {
    when(operationModeRepository.existsByName(operationModeDto.getName()))
        .thenReturn(true);

    expectedEx.expect(ValidationMessageException.class);
    expectedEx.expectMessage(
        new Message(OperationModeMessageKeys.ERROR_OPERATION_MODE_NAME_NOT_UNIQUE,
            operationModeDto.getName()).toString());

    operationModeValidator.validateUniqueConstraints(operationModeDto);
  }

  @Test
  public void shouldThrowExceptionIfOperationModeWithGivenCodeExists() {
    when(operationModeRepository.existsByCode(operationModeDto.getCode()))
        .thenReturn(true);

    expectedEx.expect(ValidationMessageException.class);
    expectedEx.expectMessage(
        new Message(OperationModeMessageKeys.ERROR_OPERATION_MODE_CODE_NOT_UNIQUE,
            operationModeDto.getCode()).toString());

    operationModeValidator.validateUniqueConstraints(operationModeDto);
  }

  @Test
  public void shouldThrowExceptionIfOperationModeWithGivenNameAndNotIdExists() {
    UUID id = UUID.randomUUID();
    when(operationModeRepository.existsByNameAndIdNot(
        operationModeDto.getName(), id))
        .thenReturn(true);

    expectedEx.expect(ValidationMessageException.class);
    expectedEx.expectMessage(
        new Message(OperationModeMessageKeys.ERROR_OPERATION_MODE_NAME_NOT_UNIQUE,
            operationModeDto.getName()).toString());

    operationModeValidator.validateUpdatedUniqueFields(operationModeDto, id);
  }

  @Test
  public void shouldThrowExceptionIfOperationModeWithGivenCodeAndNotIdExists() {
    UUID id = UUID.randomUUID();
    when(operationModeRepository.existsByCodeAndIdNot(
        operationModeDto.getCode(), id))
        .thenReturn(true);

    expectedEx.expect(ValidationMessageException.class);
    expectedEx.expectMessage(
        new Message(OperationModeMessageKeys.ERROR_OPERATION_MODE_CODE_NOT_UNIQUE,
            operationModeDto.getCode()).toString());

    operationModeValidator.validateUpdatedUniqueFields(operationModeDto, id);
  }
}
