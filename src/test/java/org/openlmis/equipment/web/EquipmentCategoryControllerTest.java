/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.equipment.web;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import  org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import org.openlmis.equipment.domain.EquipmentCategory;
import org.openlmis.equipment.dto.EquipmentCategoryDto;
import org.openlmis.equipment.repository.EquipmentCategoryRepository;
import org.openlmis.equipment.service.PermissionService;
import org.openlmis.equipment.util.EquipmentCategoryDataBuilder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


@SuppressWarnings({"PMD.UnusedPrivateField", "PMD.TooManyMethods"})
public class EquipmentCategoryControllerTest {
  @Mock
  Pageable pageable;

  @Mock
  EquipmentCategoryRepository repository;

  @InjectMocks
  EquipmentCategoryController controller = new EquipmentCategoryController();

  @Mock
  private PermissionService permissionService;

  private final String name;
  private final String code;
  private final EquipmentCategory equipmentCategory;
  private final Set<EquipmentCategory> equipmentCategories;
  private final EquipmentCategoryDto equipmentCategoryDto;

  /**
   * Constructor for test.
   */

  public EquipmentCategoryControllerTest() {
    initMocks(this);
    name = "name1";
    code = "code1";
    equipmentCategory = new EquipmentCategoryDataBuilder()
            .withName(name).build();
    equipmentCategoryDto = new EquipmentCategoryDto();
    equipmentCategory.export(equipmentCategoryDto);
    equipmentCategories = Sets.newHashSet(equipmentCategory);
  }

  @Test
  public void shouldGetAllCategory() {
    List<EquipmentCategoryDto> expected = Lists.newArrayList(equipmentCategoryDto);
    when(repository.findAll()).thenReturn(equipmentCategories);

    Page<EquipmentCategoryDto> equipmentCategoryDtos = controller.findAll(pageable);

    assertThat(equipmentCategoryDtos.getTotalElements())
            .isEqualTo(expected.size());
  }

  @Test
  public void shouldCreateNewEquipmentCategory() {
    when(repository.save(any())).thenReturn(equipmentCategory);
    controller.create(equipmentCategoryDto);
    verify(repository).save(equipmentCategory);
  }

  @Test
  public void shouldGetEquipmentCategory() {
    //given
    when(repository.findById(equipmentCategory.getId()))
            .thenReturn(Optional.of(equipmentCategory));
    EquipmentCategoryDto equipmentCategoryDto1 =
            controller.get(equipmentCategory.getId());
    assertEquals(equipmentCategoryDto1, equipmentCategoryDto);
  }


  @Test
  public void shouldNotCreateExistingEquipmentCategory() {
    when(repository.save(any())).thenReturn(equipmentCategory);
    controller.create(equipmentCategoryDto);
  }


}