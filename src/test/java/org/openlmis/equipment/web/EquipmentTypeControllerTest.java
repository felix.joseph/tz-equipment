/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.equipment.web;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import  org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.openlmis.equipment.domain.EquipmentType;
import org.openlmis.equipment.dto.EquipmentTypeDto;
import org.openlmis.equipment.repository.EquipmentTypeRepository;
import org.openlmis.equipment.service.PermissionService;
import org.openlmis.equipment.util.EquipmentTyeDataBuilder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

@SuppressWarnings({"PMD.UnusedPrivateField", "PMD.TooManyMethods"})
public class EquipmentTypeControllerTest {

  @Mock
  private Pageable pageable;

  @Mock
  private BaseController baseController;

  @Mock
  private EquipmentTypeRepository repository;

  @Mock
  private PermissionService permissionService;

  @InjectMocks
  private EquipmentTypeController controller = new EquipmentTypeController();

  private String name1;
  private String code1;
  private EquipmentType equipmentType1;
  private Set<EquipmentType> equipmentTypes;
  private EquipmentTypeDto equipmentTypeDto1;



  /**
   * Constructor for test.
   */
  public EquipmentTypeControllerTest() {
    initMocks(this);

    code1 = "code1";
    name1 = "name1";
    equipmentType1 = new EquipmentTyeDataBuilder()
            .withName(name1).build();

    equipmentTypeDto1 = new EquipmentTypeDto();
    equipmentType1.export(equipmentTypeDto1);

    equipmentTypes = Sets.newHashSet(equipmentType1);
  }


  @Test
  public void shouldGetAllTypes() {
    List<EquipmentTypeDto> expected = Lists.newArrayList(equipmentTypeDto1);
    when(repository.findAll()).thenReturn(equipmentTypes);

    Page<EquipmentTypeDto> equipmentTypeDtos = controller.findAll(pageable);

    assertThat(equipmentTypeDtos.getTotalElements())
            .isEqualTo(expected.size());
  }

  @Test
  public void shouldGetEquipmentType() {
    //given
    when(repository.findById(equipmentType1.getId()))
            .thenReturn(Optional.of(equipmentType1));
    EquipmentTypeDto equipmentTypeDto =
            controller.get(equipmentType1.getId());
    assertEquals(equipmentTypeDto, equipmentTypeDto1);
  }


  @Test
  public void shouldCreateNewEquipmentType() {
    when(repository.save(any())).thenReturn(equipmentType1);
    controller.create(equipmentTypeDto1);
    verify(repository).save(equipmentType1);
  }

  @Test
  public void shouldNotCreateExistingEquipmentType() {
    when(repository.save(any())).thenReturn(equipmentType1);
    controller.create(equipmentTypeDto1);
  }

}