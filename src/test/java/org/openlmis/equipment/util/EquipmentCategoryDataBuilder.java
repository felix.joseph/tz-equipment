/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */


package org.openlmis.equipment.util;

import java.util.UUID;

import org.openlmis.equipment.domain.Discipline;
import org.openlmis.equipment.domain.EquipmentCategory;
import org.openlmis.equipment.domain.EquipmentType;


public class EquipmentCategoryDataBuilder {
  private static int instanceNumber = 0;
  private UUID id;
  private String name;
  private String code;
  private Boolean active;
  private Integer displayOrder;
  private EquipmentType equipmentType;
  private Discipline discipline;

  /**
   * Builds instance of {@link EquipmentCategoryDataBuilder} with sample data.
   */
  public EquipmentCategoryDataBuilder() {
    instanceNumber++;

    id = UUID.randomUUID();
    name = "name" + instanceNumber;
    code = "code" + instanceNumber;
    active = true;

  }

  /**
   * Builds instance of {@link EquipmentCategory}.
   */
  public EquipmentCategory build() {
    EquipmentCategory equipmentCategory = EquipmentCategory.newEquipmentCategory(name, code,
            displayOrder, active, equipmentType, discipline);
    equipmentCategory.setId(id);
    return equipmentCategory;
  }

  /**
   * Builds instance of {@link EquipmentCategory} without id.
   */
  public EquipmentCategory buildAsNew() {
    return this.withoutId().build();
  }

  public EquipmentCategoryDataBuilder withoutId() {
    this.id = null;
    return this;
  }

  public EquipmentCategoryDataBuilder withDisplayOrder(Integer displayOrder) {
    this.displayOrder = displayOrder;
    return this;
  }

  public EquipmentCategoryDataBuilder withName(String name) {
    this.name = name;
    return this;
  }

  public EquipmentCategoryDataBuilder withCode(String code) {
    this.code = code;
    return this;
  }

  public EquipmentCategoryDataBuilder withActive(Boolean active) {
    this.active = active;
    return this;
  }



}
