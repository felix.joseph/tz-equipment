/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.equipment.util;

import java.util.UUID;
import org.openlmis.equipment.domain.EquipmentType;

public class EquipmentTyeDataBuilder {
  private static int instanceNumber = 0;

  private UUID id;
  private String name;
  private String code;
  private Boolean active;
  private Boolean isBioChemistry;
  private Boolean isColdChain;

  /**
   * Builds instance of {@link EquipmentTyeDataBuilder} with sample data.
   */
  public EquipmentTyeDataBuilder() {
    instanceNumber++;

    id = UUID.randomUUID();
    name = "name" + instanceNumber;
    code = "code" + instanceNumber;
    active = true;

  }

  /**
   * Builds instance of {@link EquipmentType}.
   */
  public EquipmentType build() {
    EquipmentType equipmentType = EquipmentType.newEquipmentType(name, code,
            isBioChemistry, isColdChain, active);
    equipmentType.setId(id);
    return equipmentType;
  }

  /**
   * Builds instance of {@link EquipmentType} without id.
   */
  public EquipmentType buildAsNew() {
    return this.withoutId().build();
  }

  public EquipmentTyeDataBuilder withoutId() {
    this.id = null;
    return this;
  }

  public EquipmentTyeDataBuilder withName(String name) {
    this.name = name;
    return this;
  }

  public EquipmentTyeDataBuilder withCode(String code) {
    this.code = code;
    return this;
  }

  public EquipmentTyeDataBuilder withActive(Boolean active) {
    this.active = active;
    return this;
  }

  public EquipmentTyeDataBuilder withColdChain(Boolean isColdChain) {
    this.isColdChain = isColdChain;
    return this;
  }

  public EquipmentTyeDataBuilder withBioChemistry(Boolean isBioChemistry) {
    this.isBioChemistry = isBioChemistry;
    return this;
  }
}